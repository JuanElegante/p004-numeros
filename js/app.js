function llenar(){
    var lim = document.getElementById('lim').value;
    var listaNum = document.getElementById('num');
    let par = document.getElementById('Pares');
    let imp = document.getElementById('Impares');
    let sim = document.getElementById('Simetrico');
    var arreglo = [];

    while(listaNum.options.length>0){
        listaNum.remove(0);
    }

    for(let con = 0; con < lim; con++) {
        let aleat = Math.floor(Math.random()*(50)+1); 
        listaNum.options[con] = new Option(aleat, 'valor:' + con);
        arreglo[con] = aleat;
     
    }
    
    let orden = ordenarValores(arreglo);

    for(let con = 0; con<lim; con++){
        listaNum.options[con] = new Option(orden[con]);
    }

    par.innerHTML = valorPar(arreglo).toFixed(2) + "%"; 
    imp.innerHTML = valorImpar(arreglo).toFixed(2) + "%"; 

    
    let pares = valorPar(arreglo);
    let impares = valorImpar(arreglo);

    if(pares > 75 || impares >75){
        sim.innerHTML = "No simetrico";
    }else {
        sim.innerHTML = "simetrico";
    }
    /* 
    Hacer commit 'Generacion de numeros aleatorios -listo
    Hacer commit 'Validacion de caja de texto (limite) REQUERIDO Y NUMERICO
    Hacer commit con la listanumeros ORDENADOS ASCENDENTE
     */
}

function alerta(){
    valor = document.getElementById('lim').value;

    errorValor = document.getElementById('errorLimite');

    valor== "" ? errorValor.style.visibility = 'visible': errorValor.style.visibility = 'hidden' ;

    if(valor==0){

        alert("Faltan datos por capturar");

    }
}

// Este ordenara de Menor a Mayor
function ordenarValores(numeros){
    let arr = numeros, longitudOrden = numeros.length;
    let band = false;

    while(!band){
        band = true;
        for(let i=0; i<longitudOrden; i++){
            if(arr[i] > arr[i+1]){
                aux = arr[i+1];
                arr [i+1] = arr [i];
                arr[i] = aux;
                band = false;
            }
        }
    }
    return arr;
}
/* Contar los numeros pares e impares para saber su porcentaje
la diferencia no sea mayor al 25%
Contar numeros pares e impartes

*/
//Contara los numeros Pares
function valorPar(numeros){
    let contador = 0; 
    let arr = numeros;
    let numerosL = numeros.length;
    for(let i=0; i<numerosL; i++){
        if(arr[i]%2 == 0){ 

            contador++;
        }
    }
    contador = ((contador * 100) / numerosL);
    return contador;

    

}

//Contara los numeros Impares
function valorImpar(numeros){
    let contador = 0; 
    let arr = numeros;
    let numerosL = numeros.length;
    for(let i=0; i<numerosL; i++){
        if(arr[i]%2 != 0){ 

            contador++;
        }
    }
    contador = ((contador * 100) / numerosL);
    return contador;

}
